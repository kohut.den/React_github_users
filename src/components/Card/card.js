import React, {Component} from 'react'
import './style.css'

var $ = require("jquery");

class Card extends Component {

  state = {
  }

  componentWillMount() {
    let {login} = this.props
    $.get("https://api.github.com/users/" + login)
    .done((data) => this.setState(data))
    .fail(() => alert('Sorry User is undefined'))
  }

  render() {
    console.log('init card')
    let {name, avatar_url, created_at} = this.state
    let time = (new Date(Date.parse(created_at))).toLocaleDateString()
    const body = created_at ? <div className="card">
                                <h1>{name}</h1>
                                <img src={avatar_url}/>
                                <p>Since at: {time}</p>
                              </div>
                            : null
    return(
      body
    )
  }
}

export default Card
