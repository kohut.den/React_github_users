import React, {Component} from 'react'

class Form extends Component {

  state={
    value: ''
  }


  render() {
    let {card} = this.props
    return(
      <form onSubmit={this.newCard}>
        <input onChange={this.handleValue} type="text" value={this.state.value}/>
        <button>Add</button>
      </form>
    )
  }

  handleValue = e => {
    this.setState({
      value: e.target.value
    })
  }

  newCard = e => {
    e.preventDefault();
    let {value} = this.state
    if (value) {
      this.props.card(value)
      this.setState({
        value:''
      })
    }
  }
}

export default Form
