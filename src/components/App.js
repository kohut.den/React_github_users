import React, { Component, PropTypes } from 'react'
import Card from './Card/card.js'
import Form from './Form/form.js'

class App extends Component {

  state = {
    logins: []
  }

  render() {
    window.state = this.state
    let {logins} = this.state
    let users = logins.map((login, index) => <Card key={index} login={login}/>)
    return (
      <div>
        <Form card={this.addCart}/>
        {users}
      </div>
    )
  }

  addCart = value => {
    this.setState ({
      logins: this.state.logins.concat(value)
    })
  }
}

export default App

// <Card login="kohut-denis"/>
// <Card login="dmitrychekalin"/>
// <Card login="VolkovBro1997"/>
// <Card login="alex-bogomolov"/>
