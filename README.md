![Image alt](https://gitlab.com/kohut.den/React_github_users/raw/master/github_users.png)

TODO List:

1 - Include Redux ✔

2 - Shift logic to redux(create reducers)

3 - Fix bug when query fail (redux will need to help me) ✔

4 - Add Drag&Drop

5 - Delete Users

6 - Add multiselect for team codica

7 - Find by user_name in the screen

8 - Add which time in the github

9 - Added reselect (need redux for this)(not need to render each <Card/> of any action)

10 - Not render <Cart/> with empty value ✔

11 Not render empty <Cart/>
